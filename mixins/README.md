##Getting Started
After you've forked and cloned the repository, open a terminal/command prompt and do the following:

1. `cd` to the directory
2. run `npm install`
3. run `grunt`
4. Copy the file to your current project
3. ???
4. Profit

##Creating Mixins and Functions
This will get you the existing mixins/functions/modules. If you want to create your own, do the following:

1. Create a new file in the `src/[functions|mixins|modules]` directory
  * Each feature/module should be a separate file
2. In the new file, the first line after declaring the function/mixin should be a comment with what the function does, and how it works. Open any file to see an example. These comments will show up in the generated file so that it's clear how to use them while working on your website. They will not show up in the generated site.css file though.
3. Run `grunt` in the terminal to build the new file full file

##Creating Modules
Follow the same steps as creating a mixin/function. Beyond that, follow smacss module method when creating these modules; favor classes over qualified element selectors. A module needs to be able to stand alone. No matter where it's placed, it needs to work either automatically, or by adding an additional class. When creating a module, you can skip the 2nd step in favor of a readme file with [markdown](http://daringfireball.net/projects/markdown/syntax) for syntax highlighting.

##Problem with a mixin?
Report it as a bug in the issue tracker. When the bug is done, make sure to link the issue ID in the commit message

##Want to request a feature?
Add an enhancement issue in the issue tracker.
###To The Developer
When the enhancement is done, make sure to link the issue ID in the commit message
